#ifndef BIGINTBASICTEST_H_INCLUDE_GUARD
#define BIGINTBASICTEST_H_INCLUDE_GUARD

namespace big
{
	void StringTest();
	void FibonacciTest();
	void FactorialTest();
	void ShiftTest();
	void SignTest();
}

#endif // BIGINTBASICTEST_H_INCLUDE_GUARD