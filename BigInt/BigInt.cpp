#include "BigInt.h"

#include <limits>
#include <sstream>
#include <cassert>

namespace big 
{
	static const UInteger	UINTEGER_MAX		= std::numeric_limits<UInteger>::max();
	static const int		UINTEGER_DIGITS		= std::numeric_limits<UInteger>::digits;

	BigInt::BigInt(signed long long int i_integer)
		: m_number(std::deque<UInteger>(1, 0)), m_negative(i_integer < 0 ? true : false)
	{
		if (i_integer < 0) i_integer *= -1;
		LongUInteger number = static_cast<LongUInteger>(i_integer);

		LongUInteger first_digit = (number & static_cast<LongUInteger>(UINTEGER_MAX));
		
		assert(!(first_digit > static_cast<LongUInteger>(UINTEGER_MAX)));
		m_number.at(0) = static_cast<UInteger>(first_digit);
		
		LongUInteger second_digit = number >> UINTEGER_DIGITS;
		if (second_digit != 0)
		{
			m_number.push_back(static_cast<UInteger>(second_digit));
		}
	}

	BigInt::BigInt(const std::string& i_number)
		: m_number(std::deque<UInteger>(1, 0)), m_negative(i_number.at(0) != '-' ? false : true)
	{
		UInteger digit	= 0;

		size_t significative_level = 0;
		for (std::string::const_reverse_iterator current_digit_iterator = i_number.rbegin();
			current_digit_iterator < i_number.rend(); ++current_digit_iterator)
		{
			char d = (*current_digit_iterator);
			
			if (d != '-')
			{
				BigInt tmp = static_cast<UInteger>(d - 48);

				for (size_t i = 0; i < significative_level; ++i)
					tmp *= 10;

				*this += tmp;

				++significative_level;
			}
		}
	}

	BigInt& BigInt::operator+=(const BigInt& i_integer)
	{
		if (CompareSigns(i_integer))
		{
			// Numbers are both positive or negative. Ok.
			LongUInteger carry = 0;
			LongUInteger tmp = 0;

			// We fill the number with a serier of 0-digits in order to
			// reach the size of the second number.
			if (m_number.size() < i_integer.m_number.size())
			{
				size_t size_diff = i_integer.m_number.size() - m_number.size();

				for (size_t i = 0; i < size_diff; ++i)
				{
					m_number.push_back(0);
				}
			}
		
			for (size_t i = 0; i < m_number.size(); ++i)
			{
				tmp = carry + static_cast<LongUInteger>(m_number.at(i));

				if (i < i_integer.m_number.size())
					tmp += static_cast<LongUInteger>(i_integer.m_number.at(i));

				// Calculate the partial result. We'll use this relationship: x % 2^n == x & (2^n - 1).
				LongUInteger digit = (tmp & static_cast<LongUInteger>(UINTEGER_MAX));

				m_number.at(i) = static_cast<UInteger>(digit);

				// Calculate carry for the next step.
				carry = (tmp >> UINTEGER_DIGITS); // The result is tmp divided by 2^n.
			}

			if (carry != 0)
			{
				// Add the last carry. This will be a new digit for the number.
				m_number.push_back(static_cast<UInteger>(carry));
			}
		}
		else 
		{
			// Sum with one positive number and one negative number.
			// This is a difference...let's use operator-=! :D
			if (abs(*this) < abs(i_integer))
			{
				// Calculate i_integer - *this and change the sign.
				BigInt difference = i_integer;
				difference.Subtract(*this);
				*this = difference;
				m_negative = i_integer.m_negative;
			}
			else 
			{
				// Calculate *this - i_integer (the sign is maintained).
				Subtract(i_integer);
			}
		}

		return *this;
	}

	BigInt& BigInt::operator-=(const BigInt& i_integer)
	{
		// Switch i_integer's sign and sum.
		*this += i_integer.Opposite(); // Or, eq. operator+=(i_integer);
		return *this;
	}

	/*	
		This operation is performed with the basic algorithm (Grid Method). A global
		performance imporvement can be reached using other techniques, such as
		Karatsuba Algorithm. 
		http://mathworld.wolfram.com/KaratsubaMultiplication.html	
	*/
	BigInt& BigInt::operator*=(const BigInt& i_integer) 
	{
		BigInt sum = 0;

		LongUInteger tmp	= 0;
		LongUInteger carry	= 0;

		// For each 'digit' of the second integer...
		for (size_t index_second = 0; index_second < i_integer.m_number.size(); ++index_second)
		{
			// If this digit is equal to 0 we don't need to calculate anything.
			if (i_integer.m_number.at(index_second) != 0)
			{
				BigInt partial;
				partial.ClearNumber();

				// ...and for each 'digit' of this number, do.
				for (size_t index_first = 0; index_first < m_number.size(); ++index_first)
				{
					tmp = static_cast<LongUInteger>(i_integer.m_number.at(index_second)) * 
						static_cast<LongUInteger>(m_number.at(index_first)) + carry;

					// We'll use this relationship: x % 2^n == x & (2^n - 1).
					LongUInteger digit = (tmp & static_cast<LongUInteger>(UINTEGER_MAX));

					partial.m_number.push_back(static_cast<UInteger>(digit));

					// Calculate carry for the next step.
					carry = (tmp >> UINTEGER_DIGITS); // The result is tmp divided by 2^n.
				}

				if (carry != 0) 
				{
					partial.m_number.push_back(static_cast<UInteger>(carry));
					carry = 0;
				}

				// Fill the 0-fields of the number.
				for (size_t i = 0; i < index_second; ++i)
				{
					partial.m_number.push_front(0);
				}

				sum += partial;
			}
		}

		m_number = sum.m_number;

		// Now, we'll set the sign.
		if (CompareSigns(i_integer))
			m_negative = false;
		else
			m_negative = true;

		return *this;
	}

	BigInt& BigInt::operator/=(const BigInt& i_integer)
	{
		// Check we'are not dividing by 0.
		assert(i_integer != 0);

		if (i_integer.m_number.size() == 1)
			DivideByOneDigit(i_integer);
		else 
			DivideBy(i_integer);

		return *this;
	}

	BigInt& BigInt::operator%=(const BigInt& i_integer)
	{
		// Check we'are not dividing by 0.
		assert(i_integer != 0);

		BigInt modulo;

		if (i_integer.m_number.size() == 1)
			modulo = DivideByOneDigit(i_integer);
		else 
			modulo = DivideBy(i_integer);

		*this = modulo;

		return *this;
	}

	BigInt& BigInt::operator++()
	{
		*this += 1;
		return *this;
	}

	BigInt BigInt::operator++(int)
	{
		BigInt tmp = *this;
		*this += 1;
		return tmp;
	}

	BigInt& BigInt::operator--()
	{
		*this -= 1;
		return *this;
	}

	BigInt BigInt::operator--(int)
	{
		BigInt tmp = *this;
		*this -= 1;
		return tmp;
	}

	BigInt& BigInt::operator<<=(int i_shiftAmount)
	{
		while (!(i_shiftAmount < UINTEGER_DIGITS))
		{
			UInteger last_digit = m_number.back();

			if (m_number.size() != 1) {
				// Make a shift of UINTEGER_DIGITS positions
				for (size_t i = 0; i < m_number.size() - 1; ++i)
				{
					size_t index = m_number.size() - (i + 1); 			
					m_number.at(index) = m_number.at(index - 1);
				}
			}

			m_number.push_back(last_digit);
			m_number.front() = 0;

			i_shiftAmount -= UINTEGER_DIGITS;
		}

		if (i_shiftAmount != 0) 
		{
			LongUInteger current = 0;
			LongUInteger old = 0;

			for (size_t i = 0; i < m_number.size(); ++i)
			{
				current = static_cast<LongUInteger>(m_number.at(i));
				current <<= i_shiftAmount;
				current += old;
				m_number.at(i) = static_cast<UInteger>(current);
				current >>= UINTEGER_DIGITS;
				old = current;
			}

			if (old != 0)
				m_number.push_back(static_cast<UInteger>(old));
		}

		return *this;
	}

	BigInt& BigInt::operator>>=(int i_shiftAmount)
	{
		while (!(i_shiftAmount < UINTEGER_DIGITS))
		{
			if (m_number.size() != 1)
				m_number.pop_front();
			else 
				m_number.front() = 0;

			i_shiftAmount -= UINTEGER_DIGITS;
		}

		if (i_shiftAmount != 0) 
		{
			LongUInteger current = 0;
			LongUInteger old = 0;

			for (size_t i = 0; i < m_number.size(); ++i)
			{
				size_t index = m_number.size() - (i + 1);

				current = static_cast<LongUInteger>(m_number.at(index));
				current <<= UINTEGER_DIGITS;
				current >>= i_shiftAmount;
				current += old;
				m_number.at(index) = static_cast<UInteger>(current >> UINTEGER_DIGITS);
				current <<= UINTEGER_DIGITS;
				old = current;
			}

			if (m_number.back() == 0)
				m_number.pop_back();
		}

		return *this;
	}

	bool BigInt::CompareSigns(const BigInt& i_integer) const
	{
		return (m_negative == i_integer.m_negative);
	}

	BigInt BigInt::Opposite() const
	{
		BigInt opposite = *this;
		opposite.m_negative = !opposite.m_negative;
		return opposite;
	}

	void BigInt::ToOpposite()
	{
		m_negative = !m_negative;
	}

	bool BigInt::EqualsTo(const BigInt& i_integer) const
	{
		if (m_number.size() != i_integer.m_number.size() || m_negative != i_integer.m_negative)
		{
			return false;
		}
		else 
		{
			// This number and i_intger has a deque of the same size.
			// Let's check if their contained values are equals or not.
			for (size_t i = 0; i < m_number.size(); ++i)
			{
				if (m_number.at(i) != i_integer.m_number.at(i))
				{
					return false;
				}
			}
			return true;
		}
	}

	bool BigInt::LessThan(const BigInt& i_integer) const
	{
		if (!CompareSigns(i_integer))
		{
			if (m_negative)
				return true;
			else
				return false;
		}
		else
		{
			if (m_number.size() != i_integer.m_number.size())
			{
				if (m_number.size() < i_integer.m_number.size())
				{
					if (m_negative) return false;
					else return true;
				}
				else 
				{
					if (m_negative) return true;
					else return false;
				}
			}
			else 
			{
				for (size_t i = 0; i < m_number.size(); ++i)
				{
					if (m_number.at(m_number.size() - (i + 1)) <
						i_integer.m_number.at(m_number.size() - (i + 1)))
					{
						if (m_negative) return false;
						else return true;
					}
					else 
					{
						if (m_number.at(m_number.size() - (i + 1)) >
							i_integer.m_number.at(m_number.size() - (i + 1)))
						{
							if (m_negative) return true;
							else return false;
						}
					}
				}

				return false;
			}
		}
	}

	bool BigInt::GreaterThan(const BigInt& i_integer) const
	{
		if (!CompareSigns(i_integer))
		{
			if (m_negative)
				return false;
			else
				return true;
		}
		else
		{
			if (m_number.size() != i_integer.m_number.size())
			{
				if (m_number.size() < i_integer.m_number.size())
				{
					if (m_negative) return true;
					else return false;
				}
				else 
				{
					if (m_negative) return false;
					else return true;
				}
			}
			else 
			{
				for (size_t i = 0; i < m_number.size(); ++i)
				{
					if (m_number.at(m_number.size() - (i + 1)) <
						i_integer.m_number.at(m_number.size() - (i + 1)))
					{
						if (m_negative) return true;
						else return false;
					}
					else 
					{
						if (m_number.at(m_number.size() - (i + 1)) >
							i_integer.m_number.at(m_number.size() - (i + 1)))
						{
							if (m_negative) return false;
							else return true;
						}
					}
				}

				return true;
			}
		}
	}

	std::string BigInt::ToString() const 
	{
		BigInt printable = abs(*this);
		bool negative = m_negative;
		std::stringstream ss;

		if (printable != 0)
		{
			while (printable != 0)
			{
				ss << printable.DivideByOneDigit(10);
			}

			if (negative)
			{
				ss << "-";
			}
		}
		else
		{
			ss << 0;
		}

		std::string number = ss.str();
		std::reverse(number.begin(), number.end());

		return number;
	}

	bool BigInt::IsEven() const
	{
		BigInt tmp = *this;
		tmp %= 2;
		if (tmp == 0) return true;
		return false;
	}

	bool BigInt::IsOdd() const
	{
		return !(IsEven());
	}

	void BigInt::Subtract(const BigInt& i_integer)  
	{
		// IMPORTANT! We assume that this object represents a number
		// greater than i_integer.
		assert(!(abs(*this) < abs(i_integer)));

		// Ok, we can subtract without change the sign.
		for (size_t i = 0; i < i_integer.m_number.size(); ++i)
		{
			if (m_number.at(i) < i_integer.m_number.at(i))
			{
				if (m_number.at(i + 1) != 0)
				{
					// I must request a loan. Note that the relationship i_integer <= *this
					// make this operation always consistent.
					--m_number.at(i + 1); 
				}
				else 
				{
					size_t zeros_index = i + 1;
					for ( ; m_number.at(zeros_index) == 0; ++zeros_index)
					{
						m_number.at(zeros_index) = UINTEGER_MAX;
					}
					--m_number.at(zeros_index);
				}

				// Now I can subtract, using a tmp.
				LongUInteger tmp = static_cast<LongUInteger>(UINTEGER_MAX) + 1 + 
						static_cast<LongUInteger>(m_number.at(i)) - static_cast<LongUInteger>(i_integer.m_number.at(i));

				m_number.at(i) = static_cast<UInteger>(tmp);
			}
			else
			{
				// I can simply subtract.
				m_number.at(i) -= i_integer.m_number.at(i);
			}
		}

		// Before exit, we'll check if the more significative digits have been reduced to 0.
		// In this case, we can remove it from the representation.
		ClearZeros();
	}

	UInteger BigInt::DivideByOneDigit(const BigInt& i_integer)
	{
		// Check we'are not dividing by 0.
		assert(i_integer != 0 && i_integer.m_number.front() != 0);

		// IMPORTANT! If i_integer has more than one digit, only
		// the first (the less significative) will be considered 
		// in order to divide this number.
		UInteger divisor = i_integer.m_number.front();

		if (abs(*this) < divisor)
		{
			// If this number is less than divisor, it must have only one digit.
			assert(m_number.size() == 1);

			UInteger tmp = m_number.front();
			*this = 0;
			return tmp;
		}

		BigInt quotient;
		quotient.ClearNumber();

		// Get the first (more significative) digit of this number.
		LongUInteger partial_dividend = m_number.back();
		m_number.pop_back();

		if (partial_dividend < divisor)
		{
			partial_dividend <<= UINTEGER_DIGITS;
			partial_dividend += m_number.back();
			m_number.pop_back();
		}

		LongUInteger digit = partial_dividend / static_cast<LongUInteger>(divisor);

		quotient.m_number.push_front(static_cast<UInteger>(digit));
		partial_dividend = partial_dividend % static_cast<LongUInteger>(divisor);

		while (!m_number.empty())
		{
			// Shift this partial result.
			partial_dividend <<= UINTEGER_DIGITS;
			partial_dividend += m_number.back();
			m_number.pop_back();

			digit = partial_dividend / static_cast<LongUInteger>(divisor);

			quotient.m_number.push_front(static_cast<UInteger>(digit));
			partial_dividend = partial_dividend % static_cast<LongUInteger>(divisor);
		}

		m_number = quotient.m_number;

		// Now, we'll set the sign.
		if (CompareSigns(i_integer))
			m_negative = false;
		else
			m_negative = true;

		return static_cast<UInteger>(partial_dividend);
	}

	/*	
		The division between BigInt is performed with an algorithm based on
		the simple grid division. For each step, the digit of the quotient
		is find with an exponential search (using powers of 2).
		A more efficient (and complex!) technique is explained and demonstrated
		here:
		http://kanooth.com/blog/2009/08/implementing-multiple-precision-arithmetic-part-2.html
	*/
	BigInt BigInt::DivideBy(const BigInt& i_integer)
	{
		// Check we'are not dividing by 0.
		assert(i_integer != 0);

		// If this number (the dividend) is less in modulus than i_integer,
		// the result is 0 and the remainder is the enteire dividend.
		if (abs(*this) < abs(i_integer))
		{
			BigInt tmp = *this;
			*this = 0;
			return tmp;
		}

		BigInt quotient;
		quotient.ClearNumber();

		BigInt partial_dividend;
		partial_dividend.ClearNumber();

		BigInt remainder = 0;

		BigInt divisor = abs(i_integer);
		size_t divisor_digits = divisor.m_number.size();

		// Initialize the partial dividend for the first step.
		while (partial_dividend.m_number.size() < divisor_digits 
			|| partial_dividend < divisor)
		{
			partial_dividend.AddLessSignificativeDigit(m_number.back());
			m_number.pop_back();
		}

		while (!m_number.empty() || !(partial_dividend < divisor))
		{
			if (!m_number.empty() && partial_dividend < divisor)
			{
				if (partial_dividend == 0) partial_dividend.ClearNumber();

				partial_dividend.AddLessSignificativeDigit(m_number.back());
				m_number.pop_back();
			}

			if (!(partial_dividend < divisor))
			{
				LongUInteger scale_factor			= 0;
				LongUInteger partial_scale_factor	= 1;

				UInteger boost						= 1;

				remainder = partial_dividend - divisor;

				// Remainder is in [0, i_integer[?
				while (!(remainder < divisor) || remainder < 0)
				{
					// std::cout << "Try with a scale factor of " << scale_factor + partial_scale_factor << std::endl; 

					if (remainder < 0)
					{
						scale_factor += partial_scale_factor / 2;
						partial_scale_factor = 1;
					}
					else 
					{
						if (scale_factor + partial_scale_factor * 2 > UINTEGER_MAX)
						{
							scale_factor += partial_scale_factor;
							partial_scale_factor = 1;
						}
						else 
							partial_scale_factor *= 2;
					}

					assert(!(scale_factor + partial_scale_factor > UINTEGER_MAX));					
					boost = static_cast<UInteger>(scale_factor + partial_scale_factor);
					remainder = partial_dividend - divisor * boost;
				}

				quotient.AddLessSignificativeDigit(boost);

				partial_dividend = remainder;
			}
			else
			{
				quotient.AddLessSignificativeDigit(0);
			}
		}

		m_number = quotient.m_number;

		// Now, we'll set the sign.
		if (CompareSigns(i_integer))
			m_negative = false;
		else
			m_negative = true;

		return partial_dividend;
	}

	void BigInt::AddMoreSignificativeDigit(UInteger i_digit)
	{
		m_number.push_back(i_digit);
	}

	void BigInt::RemoveMoreSignificativeDigit()
	{
		m_number.pop_back();
	}

	void BigInt::AddLessSignificativeDigit(UInteger i_digit)
	{
		m_number.push_front(i_digit);
	}

	void BigInt::RemoveLessSignificativeDigit()
	{
		m_number.pop_front();
	}

	void BigInt::ClearNumber()
	{
		m_number.clear();
	}

	void BigInt::ClearZeros()
	{
		while (m_number.back() == 0 && m_number.size() > 1)
			m_number.pop_back();
	}

	bool BigInt::IsOk() const 
	{
		if (m_number.empty())
		{
			return false;
		}
		else 
		{
			if (EqualsTo(0))
			{
				if (m_number.size() != 1)
					return false;
				if (m_negative)
					return false;
			}

			if (m_number.size() != 1)
			{
				if (m_number.back() == 0)
				{
					return false;
				}
			}
		}
		return true;
	}

	BigInt operator+(const BigInt& i_integer)
	{
		return i_integer;
	}

	BigInt operator-(const BigInt& i_integer)
	{
		return i_integer.Opposite();
	}

	bool operator<(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		return i_integer1.LessThan(i_integer2);
	}

	bool operator>(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		return i_integer1.GreaterThan(i_integer2);
	}

	bool operator<=(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		return (i_integer1.LessThan(i_integer2) || i_integer2.EqualsTo(i_integer2));
	}

	bool operator>=(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		return (i_integer1.GreaterThan(i_integer2) || i_integer2.EqualsTo(i_integer2));
	}

	bool operator==(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		return i_integer1.EqualsTo(i_integer2);
	}

	bool operator!=(const BigInt& i_integer1, const BigInt& i_integer2) 
	{
		return !(i_integer1.EqualsTo(i_integer2));
	}

	BigInt operator+(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		BigInt tmp = i_integer1;
		tmp += i_integer2;
		return tmp;
	}

	BigInt operator-(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		BigInt tmp = i_integer1;
		tmp -= i_integer2;
		return tmp;
	}

	BigInt operator*(const BigInt& i_integer1, const BigInt& i_integer2) 
	{
		BigInt tmp = i_integer1;
		tmp *= i_integer2;
		return tmp;
	}

	BigInt operator/(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		BigInt tmp = i_integer1;
		tmp /= i_integer2;
		return tmp;
	}

	BigInt operator%(const BigInt& i_integer1, const BigInt& i_integer2)
	{
		BigInt tmp = i_integer1;
		tmp %= i_integer2;
		return tmp;
	}

	BigInt pow(const BigInt& i_base, const BigInt& i_exponent)
	{
		BigInt base		= i_base;
		BigInt exponent	= i_exponent;

		BigInt result = 1;
		while (i_exponent != 0)
		{
			if (i_exponent.IsOdd())
			{
				result = result * base;
				--exponent;
			}
			base		*= base;
			exponent	/= 2;
		}

		return result;
	}

	BigInt abs(const BigInt& i_integer)
	{
		if (i_integer < 0)
			return i_integer.Opposite();
		else return i_integer;
	}

	std::ostream& operator<<(std::ostream& out, const BigInt& i_integer)
	{
		out << i_integer.ToString();

		return out;
	}
}