#ifndef BIGINTTYPES_H_INCLUDE_GUARD
#define BIGINTTYPES_H_INCLUDE_GUARD

namespace big
{
	typedef unsigned __int32 UInteger;
	typedef unsigned __int64 LongUInteger;
}

#endif // BIGINTTYPES_H_INCLUDE_GUARD