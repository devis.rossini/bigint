#ifndef BIGINT_H_INCLUDE_GUARD
#define BIGINT_H_INCLUDE_GUARD

#include "BigIntTypes.h"

#include <deque>

#include <string>
#include <iostream>

namespace big
{
	class BigInt
	{
	public:
		BigInt(signed long long i_int = 0);

		BigInt(const std::string& i_str); 

		BigInt& operator+=(const BigInt& i_integer);
		BigInt& operator-=(const BigInt& i_integer);
		BigInt& operator*=(const BigInt& i_integer);
		BigInt& operator/=(const BigInt& i_integer);
		BigInt& operator%=(const BigInt& i_integer);

		BigInt& operator++();
		BigInt	operator++(int);

		BigInt& operator--();
		BigInt	operator--(int); 

		BigInt& operator<<=(int i_shiftAmount);
		BigInt& operator>>=(int i_shiftAmount);

		bool CompareSigns(const BigInt& i_integer) const;

		BigInt Opposite() const;
		void ToOpposite();

		bool EqualsTo(const BigInt& i_integer) const;
		bool LessThan(const BigInt& i_integer) const;
		bool GreaterThan(const BigInt& i_integer) const;

		std::string ToString() const;

		bool IsEven()	const;
		bool IsOdd()	const;

	private:
		void Subtract(const BigInt& i_integer);

		UInteger DivideByOneDigit(const BigInt& i_integer);
		BigInt DivideBy(const BigInt& i_integer);

		void AddMoreSignificativeDigit(UInteger i_digit);
		void RemoveMoreSignificativeDigit();

		void AddLessSignificativeDigit(UInteger i_digit);
		void RemoveLessSignificativeDigit();

		void ClearNumber();

		void ClearZeros();

		bool IsOk() const;

		// Number representation..
		std::deque<UInteger> m_number;
		// Is this number negative? This represents the sign of the number.
		bool				m_negative;
	};

	// Unary operators
	BigInt operator+(const BigInt& i_integer);
	BigInt operator-(const BigInt& i_integer);

	// Binary comparison operators
	bool operator<(const BigInt& i_integer1, const BigInt& i_integer2);
	bool operator>(const BigInt& i_integer1, const BigInt& i_integer2);

	bool operator<=(const BigInt& i_integer1, const BigInt& i_integer2);
	bool operator>=(const BigInt& i_integer1, const BigInt& i_integer2);
	bool operator==(const BigInt& i_integer1, const BigInt& i_integer2);
	bool operator!=(const BigInt& i_integer1, const BigInt& i_integer2);

	// Binary operations
	BigInt operator+(const BigInt& i_integer1, const BigInt& i_integer2);
	BigInt operator-(const BigInt& i_integer1, const BigInt& i_integer2);
	BigInt operator*(const BigInt& i_integer1, const BigInt& i_integer2);
	BigInt operator/(const BigInt& i_integer1, const BigInt& i_integer2);
	BigInt operator%(const BigInt& i_integer1, const BigInt& i_integer2);

	// Power
	BigInt pow(const BigInt& i_base, const BigInt& i_exponent);

	// Absolute value
	BigInt abs(const BigInt& i_integer);

	// Input/output (stream) operators
	std::ostream& operator<<(std::ostream& out, const BigInt& i_integer);
	// std::istream& operator>>(std::istream& in, BigInt& i_integer);  
}

#endif // BIGINT_H_INCLUDE_GUARD