#include "BigIntBasicTest.h"

using namespace big;

int main()
{
	StringTest();
	FibonacciTest();
	FactorialTest();
	ShiftTest();
	SignTest();

	return 0;
}